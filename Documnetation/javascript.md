# Javascript

Category: Learning
Date: April 27, 2022 → April 29, 2022
Type: Lecture

## **Udemdy course for OOP javascript**

## Primitive types

- Number
- String
- Boolean
- Null
- Undefined

```jsx
x = null
document.write(typeof x)
```

above code will give x type as an object to fix this we can use `===` to set it to Null

`===` no only compare value but the data types

```jsx
x = 123
s = "123"
x == s
x === s
```

`x == s` will return as true it will convert and then compare whereas `===` will just compare

## Methods

## String Methods

- `.toLoweCase()` convers to lower case
- `.substring(4,10)` get characters from 4 to 10 indices
- `.charAt(6)` get character at 6 index

`.toString()` can be applied on numbers as weel as boolean

## Reference type

Reference type —> object —> properties are made of name and value name is string value can be data/function

```jsx
var obj1 - new Object();
var obj2 = obj1;

obj1.myProperty = "All the power is in you.";

documnet.write(obj2.myPoperty)
```

the object just maps to the same memory location rather than creating a new data as supposed to primitive data type

## Create Object

To create an object basic keyword is `object()`  and some are:

- Array `new Array()`
- Date `new Date()`
- Error `new Error(”catch error”)`
- Function `new Function(”document.write(’hello’)”)`
- RegExp `new RegExp(”\\d+”)`

## Object Literal

```jsx
 var student ={
"name":”Bob”,
"Score": 80
}
document.write(student.name)
```

`instanceof object` instance of compare left side object to right side type and output is true or false

## Functions

## Arrow function

shortcut to define an anonymous functions

Basic arrow function

```jsx
()⇒console.log(”hello”)
x()
```

Arrow function with return

```jsx
let x = () =>{
return "Hello"
}
```

Parameter with arrow function

```jsx
let add =(num1,num2)=>{
return num1-num2:
}

let divide = (num1, num2) => }{
return num1/num2:
}

conole.log(add(2,10));
console.log(divide(10,9));
```

## Function hoisting

function hoisting order matter expression vs declaration

 when the function is defined using-declaration way i.e `function x() {}` then we can call a function before defining that is known as function hoisting

but if we use a function expression that is `var x = function () {}` we can’t call x before defining x

## passing function to other function

## Arguments

add an argument to give more than one parameter to the   function

## Overloading function

```jsx
function add (num1.num2,num3) {
return num1+num2+num3:
}

function add (num1,num2) {
return num1+num2:
}

document.write(add(2,3,4))
```

javascript will take last function with same name because javascript doesn't known a given function (see arguments tab) take how many parameter so it will take last.

## this function

```jsx
function displayDetails () {
document.write(this.id);
document.id(this.name)
}

var student1 ={
id: 1,
name: "BOB",
display:displayDetails
};

var student2 ={
id: 2,
name: "Jane",
display:displayDetails
};

this.id =3;
this.name ="Mary";

student1.displayDetails();
student2.displayDetails();
displayDetails();
```

## call,apply,bind

call is use give dyanmis to this function

```jsx
function displayDetails () {
document.write(this.id);
document.id(this.name)
}

var student1 ={
id: 1,
name: "Bob"
};

var student2 ={
id: 2,
name: "Jane"

};

this.id =3;
this.name ="Mary";

displayDetails.call(student1);
displayDetails.call(student2);
displayDetails.call();
```

`apply` take array as a argument instead of individual paramenter

```jsx
displaydetails.apply([10,20,30])
```

`bind` will return a function that we need to assign to variable

```jsx
var dispalystudent1 = dispalyDetails.bind(student1)
dispalystudent1()
```

## Types

## Dynamically typed

can change type at run time

`name= “Aasim”  name=123`

## static type

we define type before han

`string name =”Aasim`”

## Variable

Let, Const and Var

## Objects

## Properties

Internals:

- `[[Put]]` add to add object properties or add thing to object
- `[[set]]` update existing properties
- `[[deleted]]` delete property

Basics:

- to check property exists use `in`  operator in gives booelam as output
- `toString` is inherited form parent Object Object have some method by default toString is one of them
- To check object has own propertywe use `hasOwnProperty` method
- `delete.course1.description` will delete the object property called description
- every property have enumrable flag and it is set to truefor (var eachProperty in course1) {

```jsx
for (var eachProperty in course1) {
console.log(eachproperty);
}

//above code will give you property name

//to property value

console.log(course1[eachProperty])
```

 we can use `var allProperties = object.keys(course1);` to get all keys of object

```jsx
var allPoperties = object.keys(course1):
for (var i=0; I<allProperties.length; i++) {
console.log(allProperties[i]);
}
```

`console.log(corse1.propertyIsEnumerable(”name”));`

accesor property:

accessor and mutator or getter and setter

```jsx
var creditCard = {
_name: "jon",

get name(){
return this.name:
},

set name (value) {
return _name=value:
}

};

console.log(creditcard.name)

//.name will be access using gtet name function to accessing property
```

enumerable and configurable:

Configurable: it won't allow to delete or modify object property

```jsx
var creditCard = {
_name: "Bob"
};

Object.definePoperty(creditCard,"name",
{
configurable: false
});

delete creditCard.name;
console.log("name" is creditCard);
console.log(creditCard.name);

```

enumerable allows you to check whether it can be accessed using `propertyIsEnumerable` method

```jsx
var creditCard = {
_name: "Bob"
};

Object.definePoperty(creditCard,"name",
{
enumerable: false
});

console.log("name" is creditCard);
console.log(creditCrad.propertyIsEnumerable("name"));
```

by default boolean value in javascript is false

```jsx
var creditCard = {};

Object.defineProperty(creditCard,"name",{
value: "john",
enumerable: true,  //enumerable
configurable: true, //delete or modify
writable: true //modify
});
```

 the attribute is wit `:`

Object.isExtensible(product)

extensibility of an object can be control in 3 way

- non-extensible
- seal nonextensible as well as coset it to non-configurable
- freeze

we can modify data type property or acfcewsor type property

datatype property is

value

writable

enumerable

accessor type property

get

set

## Constructor and prototype

constructor is used to make a template and we can have similar object sharing sam,e constructor and prototype is used to share properties

the function name is capital its Constructor

```jsx
// normal function
function hello () {
return
}

//Constructor 
function Hello () {
return
}

function Flight () {}

var flight1 = new Flight
var flight3 = new Flight

console.log(flight1 instanceof Flight)
console.log(flight2 instanceof Flight)
```

## inheritance

## ES6

- block-level scope
- call by using the `let`
- `const` for constant variable
- template strings long and length string
- variable substitution e.g `${name}`
- De-structuring
- loops forEach for-in for-of
- map for key-value pair
- set for any value

## Modules

create a function in one file and use export to export it and implement it in another file using import

e.g libs file having to add() subtract() functions
