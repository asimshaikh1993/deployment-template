# Docker

Category: Learning
Date: April 27, 2022 → April 28, 2022
Link: <https://www.youtube.com/watch?v=3c-iBn73dDE&t=5115s>
Type: Lecture

## Idle workflow with docker

1. you write your **javascript** front end with some backend server(e.g **redis**)
2. **Git:** you have a source code repo like git hub
3. **Jenkins:** is configured to
    1. run docker build after every commit
    2. **push** it to Amazon ECR or docker repository
4. **pull** that images on the main server

## Docker V/s virtual machine

| Name | Docker | VM | explanation |
| --- | --- | --- | --- |
| Size | less | More | have OS also so the size of VM is in GB where docker is in MB |
| load time | fast | slow | need to load os so VM time is more |

## Images v/s container

images are the repo or images that are present locally where as a container are running instances of that image

## Basic commands

| Command | Expalnantion |
| --- | --- |
| docker run | run docker image as well as pull if not present locally |
| docker run -d | run the container in detached mode |
| docker start | start container for local images |
| docker stop | stop container |
| docker pull | pull the image to a local repo |
| docker images | get all local images |
| docker ps | get all running container list |
| docker ps -a | get all containers that are running or shutdown |
| docker -v host path;docker path | make volume attachemnt |

## Docker ports v/s localhost ports

docker run -p 6000:3000 redis:4

docker log container name/id

docker exec -it container name/id /bin/bash

exit to close the container terminal

docker network ls

## Docker-compose

```yaml
version: '3' # compose version
services:
  # my-app:
  # image: ${docker-registry}/my-app:1.0
  # ports:
  # - 3000:3000
  MongoDB: # name of container
    image: mongo # name of the image
    ports:
      - 27017:27017
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=password
    volumes:
      - mongo-data:/data/db
  mongo-express:
    image: mongo-express
    restart: always # fixes MongoNetworkError when mongodb is not ready when mongo-express starts
    ports:
      - 8080:8081
    environment:
      - ME_CONFIG_MONGODB_ADMINUSERNAME=admin
      - ME_CONFIG_MONGODB_ADMINPASSWORD=password
      - ME_CONFIG_MONGODB_SERVER=mongodb
volumes:
  mongo-data:
    driver: local
```

Docker-compose is like a YAML file

To run docker-compose run:

`docker-compose -f mongo.YAML -d up`

-f = file

to shutdown run:

`docker-compose -f mongo.YAML down`

it removes the container as well as the network it created

## Amazon ECR

let you have one image per repository but I can have multiple versions of the same image

## Docker volume

wherever the container is restarted data is lost to overcome this we have docker volume

1. Host volumes

    you mount the physical file system to docker virtual file system so both directory data stay in sync so when write-in host system and start container will have the same files in it

2. Anonymous volume

    docker creates a path on its own no need to specify the host path

3. Named volumes
