variable "namespace" {
  description = "Development namespace"
  default     = "TEST"
  type        = string
}

variable "region" {
  description = "AWS region"
  default     = "ap-south-1"
  type        = string
  
}