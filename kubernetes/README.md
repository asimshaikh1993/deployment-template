# Kubernetes

Category: Learning
Date: April 28, 2022
Link: <https://www.youtube.com/watch?v=X48VuDVv0do&t=2s>
Type: Lecture

## Basic components

- **pods** basic unit which have one app-based container such as a javascript app container and database container
- **services**

    they are used to communicate between pod

    they are persistent static IP address

    it also acts as a load balancer

- **ingress** to route traffic inside the cluster
- **config map**
- **secrets** use to store data such s environment variable id or password that you shouldn't store in the config map file
- **volumes**
  - local in the same pod
  - remote

you as admin use define the replica you want Kubernetes will generate pods and manage them on their own using services

DB can't be replicated because they have state

for **stateLESS app**s, **deployment** is used and for **stateFULL** apps, **stateful sets** are used

using stateful sets is hard so mostly we only deploy using deployment the stateless app and database are managed by other services

nodes are there two types master and salve

each node has multiple pods

### Each slave/worker node should have 3 processes installed

1. **container runtime** that could be docker
2. **kubelet** is used to manage resources for a node
3. **Kube proxy** use to handle services request in an intelligent and performant way

### Each master node has 4 processes

1. **API server** use to interact with for example kubectl or other cli
2. **scheduler** intelligent and performant way to schedule what component to what pod. Scheduler just decides which node to use for pod deployment pod **kubelet** processes will handle pod deployment
3. **controller** **manager** detects stage changes in each node
4. **etcd store** key-value store brain of the entire cluster

## Kubectl and minikube

minikube is used to run and provision pods its a development which have one pod that has a master as well as a client pod and use to provide a test environment whereas kubectl is a cli interface which use to talk to our kubectl cluster it can be in production od development cluster

### kubectl basic

you cannot create a pod directly u need to create a deployment that creates a pod internally

## CRUD command

| Commands | Description |
| --- | --- |
| `Kubectl get pod` |  |
| `Kubectl  get service` |  |
| `Kubectl get node` |  |
| `kubectl get deployment` |  |
| `Kubert create deployment <name of deployment> —image=<name of image>` |  |
| `kubectl get replicaset` |  |
| `kubectl edit deployment <name of depolyment>` |  |
| `kubectl logs` | after the container is created container logs |
| `kubectl describe pod <podname>` | get initial setup information like pulling an image etc |
| `kubectl delete deployment name of depolyment` | delete all pods, replicaseteverthing |
| `kubectl apply -f <filename.yaml>` |  |

### Debuggind commands

| `kubectl exec -it podname— bin/bash sh` |  |
| --- | --- |

## Yaml file

initial we give API version

kind deployment or a service

have 3 parts

1. metadata

    basic information such s name and label

2. specification

    specification defines for

3. status
