# MongoDB in replica set with authentication

1. To generate a keyfile `openssl rand -base64 768 > keyfile.txt`
2. Run docker compose file
3. ssh into mongo1 container
4. Run `rs-init.sh` script file in scrpits folder
5. Create a Clusteradmin use password as Password

  ```js
  use admin;
  db.createUser (
  {
  user: "UserAdmin",
  pwd: passwordPrompt(),
  roles: [ { role: "userAdminAnyDatabase" "readWriteAnyDatabase", db: "admin" } ]
  }
  );
  ```

6. Use mongocompoass to verify login is working and root is not allowed without password
7. Create sofline user with db

```js
db.auth( "UserAdmindb", passwordPrompt())
use admin;
db.createUser({
  user: "dbname",
  pwd: passwordPrompt(), // or cleartext password
  roles: [{ role: "dbOwner", db: "db" }],
});
```

8. Login using sofline user in mongodb
9. Use sofline connection string in code env
10. to update role
